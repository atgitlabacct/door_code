defmodule DoorCodeTest do
  use ExUnit.Case
  doctest DoorCode

  alias DoorCode.Door

  @code [1,2,3]
  @open_time 100

  test "happy path" do
    {:ok, door} = Door.start_link({@code, @code, @open_time})

    assert Door.locked?(door)
    Door.press(door, 1)
    assert Door.locked?(door)

    Door.press(door, 2)
    assert Door.locked?(door)

    Door.press(door, 3)
    assert Door.locked?(door) == false
    assert Door.unlocked?(door)

    :timer.sleep(@open_time)
    assert Door.locked?(door)
  end

  test "locks on incorrect code" do
    {:ok, door} = Door.start_link({@code, @code, @open_time})

    assert Door.locked?(door)
    Door.press(door, 1)
    assert Door.locked?(door)

    Door.press(door, 2)
    assert Door.locked?(door)

    Door.press(door, 4)
    assert Door.locked?(door) == false
  end
end
